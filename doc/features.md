Planned features
================

Steps towards feature completion for 10th December 2001:

Engine
------

- [x] Melee PvP combat
- [ ] Melee PvM combat
- [x] Ranged PvP combat
- [ ] Ranged PvM combat
- [x] Prayer
- [x] Player design
- [ ] Player classes
- [x] Player movement
- [x] Player following
- [x] Player trading
- [x] Player messaging
- [x] Player client settings
- [x] Player privacy settings
- [x] NPC spawn loading
- [ ] NPC movement
- [ ] NPC aggression
- [x] Friends list
- [x] Ignore list
- [x] Locs
- [x] Boundaries
- [x] Item spawns
- [x] Dropping items
- [x] Equipping items
- [x] Inventory
- [x] Experience and levelling up
- [x] Stat restoration
- [ ] Lua scripting
- [ ] Shops
- [ ] Item banking
- [ ] Player saving (SQLite)
- [x] Protocol: 110
- [ ] Protocol: RSA decryption (passwords are ignored)
- [ ] Protocol: 201
- [ ] Protocol: 204

Content
-------

These should all be implemented in Lua script:

### Skills

- [x] Prayer (bone burying)
- [ ] Prayer (altars)
- [ ] Magic (missiles)
- [ ] Magic (curses)
- [ ] Magic (teleportation)
- [ ] Magic (alchemy)
- [ ] Woodcutting
- [ ] Firemaking
- [ ] Crafting (pottery)
- [ ] Crafting (jewelry)
- [ ] Crafting (leatherworking)
- [ ] Crafting (wool)
- [ ] Fishing
- [ ] Cooking (fish/meat)
- [ ] Cooking (bread)
- [ ] Cooking (wine)
- [ ] Cooking (pies)
- [ ] Cooking (pizzas)
- [ ] Cooking (cake)
- [ ] Cooking (stew)
- [ ] Mining
- [ ] Smithing

### Quests

- [ ] Cook's Assistant
- [ ] Sheep Shearer
- [ ] The Restless Ghost
- [ ] Demon Slayer
- [ ] Romeo & Juliet
- [ ] Shield of Arrav
- [ ] Ernest the chicken
- [ ] Vampire Slayer
- [ ] Imp catcher
- [ ] Prince Ali Rescue
- [ ] Witch's potion
- [ ] Black knight's fortress
- [ ] Doric's quest
- [ ] The knight's sword
- [ ] Goblin diplomacy
- [ ] Pirate's treasure
- [ ] Dragon slayer

### Miscellaneous

- [ ] Brass key
- [ ] Muddy key and chest
- [ ] Hans NPC dialogue
- [ ] Silk trader NPC dialogue
- [ ] Tanner NPC dialogue
- [ ] Kebab Seller NPC dialogue
- [ ] Aggie NPC dialogue
- [ ] Certing (Miles, Niles, Giles NPCs)
- [ ] Bartender NPC dialogue
- [ ] Monk NPC dialogue
- [ ] Brother Jered NPC dialogue
- [ ] Barbarian NPC dialogue
- [ ] Man NPC dialogue
- [ ] Thrander NPC dialogue
- [ ] Bareak NPC dialogue
- [ ] Apothecary NPC dialogue
- [ ] Banker NPC dialogue
- [ ] Cook's Guild
- [ ] Champions' Guild
- [x] Alcohol (beers, wizards mind bomb)
- [x] Food (fish/meat)
- [x] Food (cabbage)
- [x] Food (bread)
- [x] Food (cake)
- [x] Food (pizzas)
- [x] Food (pies)
- [x] Food (stews)
- [ ] Food (kebab)
- [x] Food (spinach roll)
- [x] Strength Potions
- [ ] Dying capes
- [ ] Mixing dyes
- [ ] Monk of Zamorak wine aggression
